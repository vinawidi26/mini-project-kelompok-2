package id.kelompok2.miniproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.kelompok2.miniproject.model.entity.PublisherEntity;

@Repository
public interface PublisherRepository extends JpaRepository<PublisherEntity, Integer>{
    List<PublisherEntity> findByNamaPenerbitStartsWithIgnoreCase(String namaPenerbit);
    List<PublisherEntity> findByNamaPenerbit(String namaPenerbit);

    @Query (value = "select id_penerbit from tabel_penerbit where nama_penerbit = ?", nativeQuery = true)
    Integer FindIdByNamaPenerbit (String namaPenerbit);
}
