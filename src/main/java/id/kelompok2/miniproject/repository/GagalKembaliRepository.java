package id.kelompok2.miniproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.kelompok2.miniproject.model.entity.GagalKembaliEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GagalKembaliRepository extends JpaRepository<GagalKembaliEntity, Integer>{
    @Query (value = "SElECT count(id_buku_rusak) from tabel_gagal_kembali where keterangan = 'Rusak'",nativeQuery = true)
    Integer FindTotalBukuRusak();

    @Query (value = "SElECT * from tabel_gagal_kembali where keterangan = 'Rusak'",nativeQuery = true)
    List<GagalKembaliEntity> FindBukuRusak();

    @Query (value = "SElECT count(id_buku_rusak) from tabel_gagal_kembali where keterangan = 'Hilang'",nativeQuery = true)
    Integer FindTotalBukuHilang();

    @Query (value = "SElECT * from tabel_gagal_kembali where keterangan = 'Hilang'",nativeQuery = true)
    List<GagalKembaliEntity> FindBukuHilang();

    @Query(value="select * from tabel_gagal_kembali where kode_peminjaman = ?1", nativeQuery = true)
    GagalKembaliEntity findgagalbykode(String kodePeminjaman);
}
