package id.kelompok2.miniproject.repository;

import id.kelompok2.miniproject.model.entity.RequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends JpaRepository<RequestEntity, Integer> {
    @Query (value = "select id_request from tabel_request where judul_buku = ?", nativeQuery = true)
    Integer FindIdByJudulBuku (String judulBuku);

    @Query (value = "select jumlah_vote from tabel_request where id_request = ?", nativeQuery = true)
    Integer FindJumlahVote (Integer id);

    @Query (value = "select * from tabel_request where id_request = ?", nativeQuery = true)
    RequestEntity FindByIdRequest (Integer id);

    @Query (value = "select * from tabel_request order by jumlah_vote desc limit 3" , nativeQuery = true)
    List<RequestEntity> FindTerbaik ();
}
