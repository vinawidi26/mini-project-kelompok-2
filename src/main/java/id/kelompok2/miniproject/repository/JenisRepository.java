package id.kelompok2.miniproject.repository;

import id.kelompok2.miniproject.model.entity.JenisEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface JenisRepository extends JpaRepository<JenisEntity, Integer> {
    @Query(value = "select id_jenis from tabel_jenis where nama_jenis = ?", nativeQuery = true)
    Integer FindIdByNamaJenis (String namaJenis);

}
