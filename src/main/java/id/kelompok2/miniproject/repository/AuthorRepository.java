package id.kelompok2.miniproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.kelompok2.miniproject.model.entity.AuthorEntity;

@Repository
public interface AuthorRepository extends JpaRepository<AuthorEntity, Integer>{
    List<AuthorEntity> findByNamaPengarangStartsWithIgnoreCase(String namaPengarang);
    List<AuthorEntity> findByNamaPengarang(String namaPengarang);

    @Query (value = "select id_pengarang from tabel_pengarang where nama_pengarang = ?", nativeQuery = true)
    Integer FindIdByNamaPengarang (String namaPengarang);
}
