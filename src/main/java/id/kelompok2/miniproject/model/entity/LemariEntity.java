package id.kelompok2.miniproject.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="tabel_lemari")
public class LemariEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_lemari", unique = true)
    private Integer idLemari;

    @Column(name="kode_lemari")
    private String kodeLemari;

    @Column(name="lokasi")
    private String lokasi;

    @ManyToOne
    @JoinColumn(name="id_jenis")
    private JenisEntity jenisEntity;
}
