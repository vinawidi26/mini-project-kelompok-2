package id.kelompok2.miniproject.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "tabel_penerbit")
public class PublisherEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_penerbit", unique = true)
	private Integer idPenerbit;
	
	@Column(name = "nama_penerbit")
	private String namaPenerbit;
	
	@Column(name = "kontak_penerbit")
	private String kontakPenerbit;
	
	@Column(name = "alamat_penerbit")
	private String alamatPenerbit;
}
