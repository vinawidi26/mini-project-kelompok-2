package id.kelompok2.miniproject.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="tabel_jenis")
public class JenisEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_jenis", unique = true)
    private Integer idJenis;

    @Column(name="nama_jenis")
    private String namaJenis;
}
