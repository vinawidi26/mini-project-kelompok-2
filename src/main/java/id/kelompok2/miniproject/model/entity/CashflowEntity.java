package id.kelompok2.miniproject.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table (name="tabel_cashflow")
public class CashflowEntity {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_transaksi")
    private Integer idTransaksi;

    @Column (name = "jumlah_uang_masuk", nullable = false , length = 25)
    private Integer jumlahSaldo;

    @Column (name = "keterangan", nullable = false , length = 255)
    private String keterangan;

    @Column (name = "total_pemasukan", nullable = false , length = 255)
    private String totalPemasukan;
}
