package id.kelompok2.miniproject.model.entity;

import java.sql.Date;

import javax.persistence.*;

import lombok.Data;

@Entity
@Data
@Table (name = "tabel_pengembalian")
public class PengembalianEntity {
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name="id_pengembalian", unique = true)
	private Integer idPengembalian;
	@OneToOne 
	@JoinColumn (name = "id_peminjaman")
	private PeminjamanEntity idPeminjaman;
	@Column (name="tanggal_dikembalikan")
	private Date tanggalDikembalikan;
	@Column (name="keterangan")
	private String keteranganKembali;
	@Column (name="username")
	private String username;
	@Column (name="judul_buku")
	private String judulBuku;
	@Column (name="kode_peminjaman")
	private String kodePeminjaman;
//	@OneToOne
//	@JoinColumn (name = "kode_peminjaman")
//	private PeminjamanEntity kodePeminjaman;
	
}
