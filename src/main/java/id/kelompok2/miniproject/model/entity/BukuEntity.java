package id.kelompok2.miniproject.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table(name="tabel_buku")
public class BukuEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_buku", unique = true)
    private Integer idBuku;

    @Column(name="judul_buku")
    private String judulBuku;

    @ManyToOne
    @JoinColumn(name="id_pengarang")
    private AuthorEntity authorEntity;

    @ManyToOne
    @JoinColumn(name="id_penerbit")
    private PublisherEntity publisherEntity;

    @Column(name="stok")
    private Integer stok;

    @Column(name="tahun_terbit")
    private String tahunTerbit;

    @ManyToOne
    @JoinColumn(name="id_jenis")
    private JenisEntity jenisEntity;

    @ManyToOne
    @JoinColumn(name="id_lemari")
    private LemariEntity lemariEntity;

    @Column(name="harga_buku")
    private String hargaBuku;

    @Column(name = "tanggal_masuk")
    private Date tanggalMasuk;

    @Column(name = "cover")
    private String cover;
}
