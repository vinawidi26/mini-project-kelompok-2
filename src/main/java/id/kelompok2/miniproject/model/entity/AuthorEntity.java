package id.kelompok2.miniproject.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "tabel_pengarang")
public class AuthorEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pengarang", unique = true)
	private Integer idPengarang;
	
	@Column(name = "nama_pengarang")
	private String namaPengarang;
	
	@Column(name = "akun_sosmed")
	private String akunSosmed;
	
	@Column(name = "email_pengarang")
	private String emailPengarang;
}
