package id.kelompok2.miniproject.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Table (name = "tabel_guest")
public class GuestEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id_guest", nullable = false)
    private Integer idGuest;

    @Column (name = "nama", nullable = false)
    private String nama;

    @Column (name = "tujuan", nullable = false)
    private String tujuan;

    @Column (name = "tanggal_kehadiran", nullable = false)
    private Date tanggalKehadiran;
}
