package id.kelompok2.miniproject.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="tabel_vote")
public class VoteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_vote")
    private Integer idVote;

    @Column(name = "username")
    private String username;

    @ManyToOne
    @JoinColumn(name = "id_request")
    private RequestEntity requestEntity;

}
