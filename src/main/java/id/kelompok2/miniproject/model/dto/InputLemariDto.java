package id.kelompok2.miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InputLemariDto {
    private Integer idLemari;
    private String kodeLemari;
    private String lokasi;
    private String jenisBuku;
}
