package id.kelompok2.miniproject.model.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PengembalianDto {
	private Integer idPengembalian;
	private String username;
	private String judulBuku;
	private Date tanggalPeminjaman;
	private Date tanggalPengembalian;
	private Date tanggalDikembalikan;
	private String keteranganKembali;
	private String kodePeminjaman;
}
