package id.kelompok2.miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthorDto {
	private Integer idPengarang;
	private String namaPengarang;
	private String akunSosmed;
	private String emailPengarang;
}
