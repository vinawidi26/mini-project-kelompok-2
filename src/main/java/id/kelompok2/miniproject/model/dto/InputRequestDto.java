package id.kelompok2.miniproject.model.dto;

import lombok.Data;

@Data
public class InputRequestDto {
    private String judulBuku;
    private String namaPengarang;

}
