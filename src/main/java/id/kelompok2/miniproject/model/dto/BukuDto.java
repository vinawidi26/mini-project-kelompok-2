package id.kelompok2.miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BukuDto {
    private Integer idBuku;
    private String judulBuku;
    private String cover;
    private Integer idPengarang;
    private Integer idPenerbit;
    private Integer stok;
    private String tahunTerbit;
    private Integer idJenis;
    private Integer idLemari;
    private String hargaBuku;
    private Date tanggalMasuk;
}
