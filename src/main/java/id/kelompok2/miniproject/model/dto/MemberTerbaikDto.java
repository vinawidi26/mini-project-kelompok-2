package id.kelompok2.miniproject.model.dto;

import lombok.Data;

@Data
public class MemberTerbaikDto {
    private String username;
    private Integer jumlah;
}
