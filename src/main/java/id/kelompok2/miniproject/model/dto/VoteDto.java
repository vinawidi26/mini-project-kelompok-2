package id.kelompok2.miniproject.model.dto;

import lombok.Data;

@Data
public class VoteDto {
    private Integer idRequest;
    private String username;
}
