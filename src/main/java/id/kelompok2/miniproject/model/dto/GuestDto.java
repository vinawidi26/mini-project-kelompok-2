package id.kelompok2.miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GuestDto {
    private Integer idGuest;
    private String nama;
    private String tujuan;
    private Date tanggalKehadiran;

}
