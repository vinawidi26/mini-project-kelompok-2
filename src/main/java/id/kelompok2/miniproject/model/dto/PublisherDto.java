package id.kelompok2.miniproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PublisherDto {
	private Integer idPenerbit;
	private String namaPenerbit;
	private String kontakPenerbit;
	private String alamatPenerbit;
}
