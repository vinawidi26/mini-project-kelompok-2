package id.kelompok2.miniproject.controller;

import id.kelompok2.miniproject.assembler.GuestAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.GuestDto;
import id.kelompok2.miniproject.model.entity.GuestEntity;
import id.kelompok2.miniproject.repository.GuestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/guest")
@CrossOrigin(origins = "http://localhost:3000")
public class GuestController {
    @Autowired
    private GuestAssembler assembler;
    @Autowired
    private GuestRepository repository;

    @GetMapping("/semuadata")
    public DefaultResponse getAll(){
        List<GuestEntity> entityList = repository.findAll();
        List<GuestDto> dtoList = entityList.stream().map(entity -> assembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(dtoList);
    }

    @GetMapping("/hariini")
    public DefaultResponse get(){
        List<GuestEntity> entityList = repository.findByTanggalKehadiran();
        List<GuestDto> dtoList = entityList.stream().map(entity -> assembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(dtoList);
    }

    @GetMapping("/bulanini")
    public DefaultResponse getBulanIni(){
        List<GuestEntity> entityList = repository.findByTanggalKehadiranBulanIni();
        List<GuestDto> dtoList = entityList.stream().map(entity -> assembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(dtoList);
    }

    @PostMapping
    public DefaultResponse insert(@RequestBody GuestDto dto){
        GuestEntity entity = assembler.fromDto(dto);
        repository.save(entity);
        return DefaultResponse.ok(assembler.fromEntity(entity));
    }

    @GetMapping("/guestbulanini")
    public Integer getGuestBulanIni () {
        return repository.findTotalGuestBulanIni();
    }
}
