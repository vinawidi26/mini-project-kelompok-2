package id.kelompok2.miniproject.controller;

import id.kelompok2.miniproject.assembler.BookAssembler;
import id.kelompok2.miniproject.assembler.BukuAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.BukuDto;
import id.kelompok2.miniproject.model.dto.InputBukuDto;
import id.kelompok2.miniproject.model.entity.AuthorEntity;
import id.kelompok2.miniproject.model.entity.BukuEntity;
import id.kelompok2.miniproject.model.entity.PublisherEntity;
import id.kelompok2.miniproject.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/buku")
@CrossOrigin(origins = "http://localhost:3000")
public class BukuController {

    @Autowired
    private BukuAssembler assembler;

    @Autowired
    private BookAssembler bookAssembler;

    @Autowired
    private BukuRepository bukuRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private PublisherRepository publisherRepository;

    @Autowired
    private JenisRepository jenisRepository;

    @Autowired
    private LemariRepository lemariRepository;

    @PostMapping("/insert")
    public DefaultResponse insert(@RequestBody BukuDto dto) {
        BukuEntity entity = assembler.fromDto(dto);
        bukuRepository.save(entity);
        return DefaultResponse.ok(assembler.fromEntity(entity));
    }

//    @PostMapping("/insertbook")
//    public DefaultResponse insertBook(@ModelAttribute InputBukuDto inputBukuDto, @RequestParam MultipartFile multipartFile) throws IOException {
//        String cover = StringUtils.cleanPath(multipartFile.getOriginalFilename());
//        BukuEntity entity = bookAssembler.fromDto(inputBukuDto);
//        entity.setCover(cover);
//        BukuEntity save= bukuRepository.save(entity);
//        String uploadDir = "./cover/" + save.getIdBuku();
//        Path uploadPath = Paths.get(uploadDir);
//
//        if (!Files.exists(uploadPath)){
//            Files.createDirectories(uploadPath);
//        }
//
//        try (InputStream inputStream = multipartFile.getInputStream()){
//            Path filePath = uploadPath.resolve(cover);
//            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
//        } catch (IOException e){
//            throw new IOException ("Upload gagal");
//        }
//
//        return DefaultResponse.ok(assembler.fromEntity(entity));
//    }

    @PostMapping("/insertnew")
    public DefaultResponse insertNew(@RequestBody InputBukuDto inputBukuDto) {
        BukuDto dto = new BukuDto();

        dto.setJudulBuku(inputBukuDto.getJudulBuku());
        dto.setIdPengarang(authorRepository.FindIdByNamaPengarang(inputBukuDto.getNamaPengarang()));
        dto.setIdPenerbit(publisherRepository.FindIdByNamaPenerbit(inputBukuDto.getNamaPenerbit()));
        dto.setStok(inputBukuDto.getStok());
        dto.setTahunTerbit(inputBukuDto.getTahunTerbit());
        dto.setIdJenis(jenisRepository.FindIdByNamaJenis(inputBukuDto.getNamaJenis()));
        dto.setIdLemari(lemariRepository.FindIdByKodeLemari(inputBukuDto.getKodeLemari()));
        dto.setHargaBuku(inputBukuDto.getHargaBuku());
        dto.setCover(inputBukuDto.getCover());

        BukuEntity entity = assembler.fromDto(dto);
        bukuRepository.save(entity);
        return DefaultResponse.ok(assembler.fromEntity(entity));

    }

    @PostMapping("/update")
    public DefaultResponse update(@RequestBody InputBukuDto inputBukuDto) {
        BukuDto dto = new BukuDto();

        dto.setJudulBuku(inputBukuDto.getJudulBuku());
        dto.setIdPengarang(authorRepository.FindIdByNamaPengarang(inputBukuDto.getNamaPengarang()));
        dto.setIdPenerbit(publisherRepository.FindIdByNamaPenerbit(inputBukuDto.getNamaPenerbit()));
        dto.setStok(inputBukuDto.getStok());
        dto.setTahunTerbit(inputBukuDto.getTahunTerbit());
        dto.setIdJenis(jenisRepository.FindIdByNamaJenis(inputBukuDto.getNamaJenis()));
        dto.setIdLemari(lemariRepository.FindIdByKodeLemari(inputBukuDto.getKodeLemari()));
        dto.setHargaBuku(inputBukuDto.getHargaBuku());
        dto.setCover(inputBukuDto.getCover());

        BukuEntity entity = assembler.fromDto(dto);
        entity.setIdBuku(inputBukuDto.getIdBuku());
        bukuRepository.save(entity);
        return DefaultResponse.ok(assembler.fromEntity(entity));
    }

    @DeleteMapping("delete/{id}")
    public void delete(@PathVariable Integer id) {
        bukuRepository.deleteById(id);
    }

    @GetMapping("/{judul}")
    public DefaultResponse getJudul(@PathVariable String judul){
        if (bukuRepository.findByJudulBukuStartsWithIgnoreCase(judul).isEmpty() == false){
            List<BukuEntity> entityList = bukuRepository.findByJudulBukuStartsWithIgnoreCase(judul);
            List<InputBukuDto> dtoList = entityList.stream().map(entity -> bookAssembler.fromEntity(entity))
                    .collect(Collectors.toList());
            return DefaultResponse.ok(dtoList);
        } else {
            return DefaultResponse.error("Judul Buku Tidak Ditemukan");
        }
    }

    @GetMapping("/semuabuku")
    public DefaultResponse get(){
        List<BukuEntity> bukuEntityList = bukuRepository.findAll();
        List<InputBukuDto> bukuDtoList = bukuEntityList.stream().map(entity -> bookAssembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(bukuDtoList);
    }

    @GetMapping("/by-pengarang/{namaPengarang}")
    public DefaultResponse getByNamaPengarang(@PathVariable String namaPengarang) {
        if (authorRepository.findByNamaPengarangStartsWithIgnoreCase(namaPengarang).isEmpty() == false) {
            List<AuthorEntity> authorEntities = authorRepository.findByNamaPengarangStartsWithIgnoreCase(namaPengarang);
            List<BukuEntity> bukuEntities = bukuRepository.findByAuthorEntityIdPengarang(authorEntities.get(0).getIdPengarang());
            List<BukuDto> bukuDtos = bukuEntities.stream().map(entity -> assembler.fromEntity(entity))
                    .collect(Collectors.toList());
            return DefaultResponse.ok(bukuDtos);
        } else {
            return DefaultResponse.error("Daftar Buku Tidak Ditemukan.");
        }
    }

    @GetMapping("/bukubaru")
    public DefaultResponse getBukuBaru(){
        List<BukuEntity> bukuEntityList = bukuRepository.findByTanggalMasuk();
        List<InputBukuDto> bukuDtoList = bukuEntityList.stream().map(entity -> bookAssembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(bukuDtoList);
    }

    @GetMapping("/totalbuku")
    public Integer getTotalBuku() {
        return bukuRepository.findTotalBuku();
    }

    @GetMapping("/by-penerbit/{namaPenerbit}")
    public DefaultResponse getByNamaPenerbit(@PathVariable String namaPenerbit) {
        if (publisherRepository.findByNamaPenerbitStartsWithIgnoreCase(namaPenerbit).isEmpty() == false) {
            List<PublisherEntity> publisherEntities = publisherRepository.findByNamaPenerbitStartsWithIgnoreCase(namaPenerbit);
            List<BukuEntity> bukuEntities = bukuRepository.findByPublisherEntityIdPenerbit(publisherEntities.get(0).getIdPenerbit());
            List<BukuDto> bukuDtos = bukuEntities.stream().map(entity -> assembler.fromEntity(entity))
                    .collect(Collectors.toList());
            return DefaultResponse.ok(bukuDtos);
        } else {
            return DefaultResponse.error("Daftar Buku Tidak Ditemukan.");
        }
    }

    @GetMapping("/tahunterbit")
    public DefaultResponse getTahunTerbit(){
        List<BukuEntity> bukuEntityList = bukuRepository.findByTahunTerbit();
        List<BukuDto> bukuDtoList = bukuEntityList.stream().map(entity -> assembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(bukuDtoList);
    }


}
