package id.kelompok2.miniproject.controller;

import id.kelompok2.miniproject.assembler.LemariAssembler;
import id.kelompok2.miniproject.assembler.LibraryAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.InputLemariDto;
import id.kelompok2.miniproject.model.dto.LemariDto;
import id.kelompok2.miniproject.model.entity.LemariEntity;
import id.kelompok2.miniproject.repository.JenisRepository;
import id.kelompok2.miniproject.repository.LemariRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/lemari")
public class LemariController {
    @Autowired
    LemariRepository repository;
    @Autowired
    JenisRepository jenisRepository;
    @Autowired
    LemariAssembler assembler;
    @Autowired
    LibraryAssembler libraryAssembler;

    @GetMapping("/semualemari")
    public DefaultResponse get(){
        List<LemariEntity> lemariEntityList = repository.findAll();
        List<InputLemariDto> lemariDtoList = lemariEntityList.stream().map(entity -> libraryAssembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(lemariDtoList);
    }

    @GetMapping("/{jenis}")
    public DefaultResponse getByJenis(@PathVariable Integer jenis){
        List<LemariEntity> lemariEntities = repository.findAllByJenisEntity(jenis);
        List<LemariDto> dtoList = lemariEntities.stream().map(entity -> assembler.fromEntity(entity))
                .collect(Collectors.toList());
        return DefaultResponse.ok(dtoList);
    }

    @PostMapping
    public DefaultResponse insert(@RequestBody InputLemariDto inputLemariDto){
        LemariDto dto = new LemariDto();
        dto.setIdJenis(jenisRepository.FindIdByNamaJenis(inputLemariDto.getJenisBuku()));
        dto.setKodeLemari(inputLemariDto.getKodeLemari());
        dto.setLokasi(inputLemariDto.getLokasi());
        LemariEntity entity = assembler.fromDto(dto);
        repository.save(entity);
        return DefaultResponse.ok(assembler.fromEntity(entity));
    }
}
