package id.kelompok2.miniproject.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.kelompok2.miniproject.assembler.AuthorAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.AuthorDto;
import id.kelompok2.miniproject.model.entity.AuthorEntity;
import id.kelompok2.miniproject.repository.AuthorRepository;

@RestController
@RequestMapping("/author")
@CrossOrigin(origins = "http://localhost:3000")
public class AuthorController {
	@Autowired
	private AuthorRepository repository;
	@Autowired
	private AuthorAssembler assembler;
	
	@GetMapping
	public DefaultResponse get() {
		List<AuthorEntity> authorEntities = repository.findAll();
		List<AuthorDto> authorDtos = authorEntities.stream().map(pengarang -> assembler.fromEntity(pengarang))
				.collect(Collectors.toList());
		return DefaultResponse.ok(authorDtos);
	}

	@GetMapping("/{name}")
	public DefaultResponse getByName(@PathVariable String name){
		if (repository.findByNamaPengarangStartsWithIgnoreCase(name).isEmpty() == false){
			List<AuthorEntity> entityList = repository.findByNamaPengarangStartsWithIgnoreCase(name);
			List<AuthorDto> dtoList = entityList.stream().map(entity -> assembler.fromEntity(entity)).collect(Collectors.toList());
			return DefaultResponse.ok(dtoList);
		} else {
			return DefaultResponse.error("Pengarang Tidak Ditemukan");
		}
	}
	
	@PostMapping("/insert")
	public DefaultResponse insert(@RequestBody AuthorDto dto) {
		AuthorEntity authorEntity = assembler.fromDto(dto);
		repository.save(authorEntity);
		return DefaultResponse.ok(assembler.fromEntity(authorEntity));
	}

	@PostMapping("/insert2")
	public DefaultResponse insert2(@RequestBody AuthorDto dto) {
		AuthorEntity authorEntity2 = assembler.fromDto(dto);
		if (repository.findByNamaPengarang(dto.getNamaPengarang()).isEmpty() == true) {
			repository.save(authorEntity2);
			return DefaultResponse.ok(assembler.fromEntity(authorEntity2));
		} else {
			return DefaultResponse.error("Nama Pengarang telah ada pada data pengarang.");
		}
	}

	@PutMapping("/update/{idPengarang}")
	public DefaultResponse insert(@RequestBody AuthorDto dto, @PathVariable Integer idPengarang) {
		AuthorEntity authorEntity = assembler.fromDto(dto);
		authorEntity.setIdPengarang(idPengarang);
		repository.save(authorEntity);
		return DefaultResponse.ok(assembler.fromEntity(authorEntity));
	}
	
	@DeleteMapping("/delete/{idPengarang}")
	public void delete(@PathVariable Integer idPengarang) {
		repository.deleteById(idPengarang);
	}
}
