package id.kelompok2.miniproject.controller;

import java.util.List;
import java.util.stream.Collectors;

import id.kelompok2.miniproject.model.dto.PengembalianRusakDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.kelompok2.miniproject.assembler.PengembalianAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.PengembalianDto;
import id.kelompok2.miniproject.model.entity.PengembalianEntity;
import id.kelompok2.miniproject.repository.BukuRepository;
import id.kelompok2.miniproject.repository.GagalKembaliRepository;
import id.kelompok2.miniproject.repository.MemberRepository;
import id.kelompok2.miniproject.repository.PeminjamanRepository;
import id.kelompok2.miniproject.repository.PengembalianRepository;
import id.kelompok2.miniproject.service.PengembalianService;

@RestController
@RequestMapping("/pengembalian")
@CrossOrigin (origins = "http://localhost:3000")
public class PengembalianController {
	@Autowired
	private PengembalianRepository repository;
	@Autowired
	private MemberRepository memberRepository;
	@Autowired
	private PengembalianService service;
	@Autowired
	private PengembalianAssembler assembler;
	@Autowired
	private PeminjamanRepository pinjamRepository;
	@Autowired
	private BukuRepository bukuRepository;
	@Autowired 
	private GagalKembaliRepository gagalRepository;
	
	@GetMapping
	public DefaultResponse get() {
		List<PengembalianEntity> pengembalianList = repository.findAll();
		List<PengembalianDto> pengembalianDtoList = pengembalianList.stream().map(pengembalian -> assembler.fromEntity(pengembalian))
				.collect(Collectors.toList());
		return DefaultResponse.ok(pengembalianDtoList);
	}
	
	@PostMapping("/user")
	public DefaultResponse insert( @RequestBody PengembalianDto dto) {
		if (pinjamRepository.findPinjamByKode(dto.getKodePeminjaman()) == null ) {
			return DefaultResponse.error("Data Tidak Ditemukan");
		}
		else if(repository.findkembalibykode(dto.getKodePeminjaman()) != null ) {
			return DefaultResponse.error("Buku sudah dikembalikan");
		}
		else if (gagalRepository.findgagalbykode(dto.getKodePeminjaman()) != null) {
			return DefaultResponse.error("Buku sudah dikembalikan");
		}
		else if (pinjamRepository.findPinjamByKode(dto.getKodePeminjaman()).getUsername().equals(dto.getUsername()) == false) {
			return DefaultResponse.error("Username yang dimasukan salah");
		} 
		else if (pinjamRepository.findPinjamByKode(dto.getKodePeminjaman()).getJudulBuku().equals(dto.getJudulBuku()) == false) {
			return DefaultResponse.error("Judul Buku yang dimasukan salah");
		}
		else 	{
			return DefaultResponse.ok(service.insertDataPengembalian(dto.getKodePeminjaman(), dto));
		}
	}

	@PostMapping("/gagal")
	public DefaultResponse insertGagal (@RequestBody PengembalianRusakDto pengembalianRusakDtodto) {
		if (pinjamRepository.findPinjamByKode(pengembalianRusakDtodto.getKodePeminjaman()) == null) {
			return DefaultResponse.error("Data Tidak Ditemukan");
		}
		else if(repository.findkembalibykode(pengembalianRusakDtodto.getKodePeminjaman()) != null) {
			return DefaultResponse.error("Buku sudah dikembalikan");
		}
		else if (gagalRepository.findgagalbykode(pengembalianRusakDtodto.getKodePeminjaman()) != null) {
			return DefaultResponse.error("Buku sudah dikembalikan");
		}
		else if (pinjamRepository.findPinjamByKode(pengembalianRusakDtodto.getKodePeminjaman()).getUsername().equals(pengembalianRusakDtodto.getUsername()) == false) {
			return DefaultResponse.error("Username yang dimasukan salah");
		}
		else if (pinjamRepository.findPinjamByKode(pengembalianRusakDtodto.getKodePeminjaman()).getJudulBuku().equals(pengembalianRusakDtodto.getJudulBuku()) == false) {
			return DefaultResponse.error("Judul Buku yang dimasukan salah");
		} 
		else {
			return DefaultResponse.ok(service.insertDataRusak(pengembalianRusakDtodto.getKodePeminjaman(), pengembalianRusakDtodto));
		}
	}
	
}
