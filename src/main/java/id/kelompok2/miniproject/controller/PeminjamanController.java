package id.kelompok2.miniproject.controller;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import id.kelompok2.miniproject.model.dto.MemberTerbaikDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.kelompok2.miniproject.assembler.PeminjamanAssembler;
import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.PeminjamanDto;
import id.kelompok2.miniproject.model.entity.PeminjamanEntity;
import id.kelompok2.miniproject.repository.BukuRepository;
import id.kelompok2.miniproject.repository.MemberRepository;
import id.kelompok2.miniproject.repository.PeminjamanRepository;
import id.kelompok2.miniproject.service.PeminjamanService;

@RestController
@RequestMapping("/peminjaman")
@CrossOrigin (origins = "http://localhost:3000")
public class PeminjamanController {
	@Autowired
	private PeminjamanRepository repository;
	@Autowired
	private PeminjamanAssembler assembler;
	@Autowired
	private PeminjamanService service;
	@Autowired 
	private MemberRepository memberRepository;
	@Autowired
	private BukuRepository bukuRepository;

	
	@GetMapping("/user/{username}")
	public DefaultResponse getById(@PathVariable String username) {
		if(memberRepository.findIdByUname(username) == null ) {
			return DefaultResponse.error("User Tidak Ditemukan");
		} else {
		List<PeminjamanEntity> pemEntities = repository.findAllByUsername(username);
		List<PeminjamanDto> peminjamanList = pemEntities.stream().map(peminjaman -> assembler.fromEntity(peminjaman))
				.collect(Collectors.toList());
		return DefaultResponse.ok(peminjamanList);
	}
	}
	
	@GetMapping
	public DefaultResponse get() {
		List<PeminjamanEntity> peminjamanList = repository.findAll();
		List<PeminjamanDto> peminjamanDtoList = peminjamanList.stream().map(peminjaman -> assembler.fromEntity(peminjaman))
				.collect(Collectors.toList());
		return DefaultResponse.ok(peminjamanDtoList);
	}

	@GetMapping("/belumkembali")
	public DefaultResponse getBelumKembali() {
		List<PeminjamanEntity> peminjamanList = repository.findAllBelumKembali();
		List<PeminjamanDto> peminjamanDtoList = peminjamanList.stream().map(peminjaman -> assembler.fromEntity(peminjaman))
				.collect(Collectors.toList());
		return DefaultResponse.ok(peminjamanDtoList);
	}
	
	@PostMapping ("/user") 
	public DefaultResponse insert( @RequestBody PeminjamanDto dto) {
		if(memberRepository.findIdByUname(dto.getUsername()) == null ) {
			return DefaultResponse.error("User Tidak Ditemukan");
		} else if (bukuRepository.findAllByJudulBukuLike(dto.getJudulBuku()) == null) {
			return DefaultResponse.error("Buku Tidak Ditemukan");
		} else {
		return DefaultResponse.ok(service.insertDataPeminjaman(dto));
		}
	}
	
	@DeleteMapping("/{idPeminjaman}")
	public DefaultResponse delete(@PathVariable Integer idPeminjaman) {
		if(repository.findById(idPeminjaman).isPresent() == false) {
			return DefaultResponse.error("Data Tidak Ditemukan");
		} else {
		service.deleteDataPeminjaman(idPeminjaman);
		return DefaultResponse.ok("Deleted");
		}
	}

	@GetMapping("/pinjam")
	public Integer getBukuDipinjam() {
		return repository.FindTotalBukuPinjam();
	}

	@GetMapping("/terbaik")
	public List<String> getMemberTerbaik() {
		return repository.FindMemberTerbaik();
	}
}
