package id.kelompok2.miniproject.assembler;

import id.kelompok2.miniproject.model.dto.LemariDto;
import id.kelompok2.miniproject.model.entity.JenisEntity;
import id.kelompok2.miniproject.model.entity.LemariEntity;
import org.springframework.stereotype.Component;

@Component
public class LemariAssembler implements InterfaceAssembler<LemariEntity, LemariDto> {

    @Override
    public LemariEntity fromDto(LemariDto dto) {
        if (dto == null)
        return null;

        LemariEntity entity = new LemariEntity();
        JenisEntity jenisEntity = new JenisEntity();
        if (dto.getKodeLemari() != null) entity.setKodeLemari(dto.getKodeLemari());
        if (dto.getLokasi() != null) entity.setLokasi(dto.getLokasi());
        if (dto.getIdJenis() != null) jenisEntity.setIdJenis(dto.getIdJenis());
        entity.setJenisEntity(jenisEntity);
        return entity;
    }

    @Override
    public LemariDto fromEntity(LemariEntity entity) {
        if (entity == null)
        return null;
        return LemariDto.builder()
                .idLemari(entity.getIdLemari())
                .kodeLemari(entity.getKodeLemari())
                .lokasi(entity.getLokasi())
                .idJenis(entity.getJenisEntity().getIdJenis())
                .build();
    }
}
