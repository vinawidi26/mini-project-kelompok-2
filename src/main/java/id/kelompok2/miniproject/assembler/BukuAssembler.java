package id.kelompok2.miniproject.assembler;

import id.kelompok2.miniproject.model.dto.BukuDto;
import id.kelompok2.miniproject.model.entity.*;
//import id.kelompok2.miniproject.repository.BukuRepository;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;

@Component
public class BukuAssembler implements InterfaceAssembler<BukuEntity, BukuDto> {

//    @Autowired
//    private BukuRepository bukuRepository;

    @Override
    public BukuEntity fromDto(BukuDto dto) {
        if (dto == null)
            return null;

        BukuEntity entity = new BukuEntity();
        AuthorEntity authorEntity = new AuthorEntity();
        PublisherEntity publisherEntity = new PublisherEntity();
        JenisEntity jenisEntity = new JenisEntity();
        LemariEntity lemariEntity = new LemariEntity();
        if (dto.getIdBuku() != null) entity.setIdBuku(dto.getIdBuku());
        if (dto.getJudulBuku() != null) entity.setJudulBuku(dto.getJudulBuku());
        if (dto.getCover() != null) entity.setCover(dto.getCover());
        if (dto.getIdPengarang() != null) authorEntity.setIdPengarang(dto.getIdPengarang());
        if (dto.getIdPenerbit() != null) publisherEntity.setIdPenerbit(dto.getIdPenerbit());
        if (dto.getStok() != null) entity.setStok(dto.getStok());
        if (dto.getTahunTerbit() != null) entity.setTahunTerbit(dto.getTahunTerbit());
        if (dto.getIdJenis() != null) jenisEntity.setIdJenis(dto.getIdJenis());
        if (dto.getIdLemari() != null) lemariEntity.setIdLemari(dto.getIdLemari());
        if (dto.getHargaBuku() != null) entity.setHargaBuku(dto.getHargaBuku());
        LocalDate date = LocalDate.now();
        entity.setTanggalMasuk(Date.valueOf(date));
        entity.setAuthorEntity(authorEntity);
        entity.setPublisherEntity(publisherEntity);
        entity.setJenisEntity(jenisEntity);
        entity.setLemariEntity(lemariEntity);
        return entity;
    }

    @Override
    public BukuDto fromEntity(BukuEntity entity) {
        if (entity == null)
            return null;

        return BukuDto.builder()
                .idBuku(entity.getIdBuku())
                .judulBuku(entity.getJudulBuku())
                .cover(entity.getCover())
                .idPengarang(entity.getAuthorEntity().getIdPengarang())
                .idPenerbit(entity.getPublisherEntity().getIdPenerbit())
                .stok(entity.getStok())
                .tahunTerbit(entity.getTahunTerbit())
                .idJenis(entity.getJenisEntity().getIdJenis())
                .idLemari(entity.getLemariEntity().getIdLemari())
                .hargaBuku(entity.getHargaBuku())
                .tanggalMasuk(entity.getTanggalMasuk())
                .build();
    }
}
