package id.kelompok2.miniproject.assembler;

import org.springframework.stereotype.Component;

import id.kelompok2.miniproject.model.dto.AuthorDto;
import id.kelompok2.miniproject.model.entity.AuthorEntity;

@Component
public class AuthorAssembler implements InterfaceAssembler<AuthorEntity, AuthorDto> {

	@Override
	public AuthorEntity fromDto(AuthorDto dto) {
		// TODO Auto-generated method stub
		if (dto == null)
			return null;

		AuthorEntity entity = new AuthorEntity();
		if (dto.getIdPengarang() != null)
			entity.setIdPengarang(dto.getIdPengarang());
		if (dto.getNamaPengarang() != null)
			entity.setNamaPengarang(dto.getNamaPengarang());
		if (dto.getAkunSosmed() != null)
			entity.setAkunSosmed(dto.getAkunSosmed());
		if (dto.getEmailPengarang() != null)
			entity.setEmailPengarang(dto.getEmailPengarang());

		return entity;
	}

	@Override
	public AuthorDto fromEntity(AuthorEntity entity) {
		// TODO Auto-generated method stub
		if (entity == null)
			return null;
		return AuthorDto.builder().idPengarang(entity.getIdPengarang()).namaPengarang(entity.getNamaPengarang())
				.akunSosmed(entity.getAkunSosmed()).emailPengarang(entity.getEmailPengarang()).build();
	}
}
