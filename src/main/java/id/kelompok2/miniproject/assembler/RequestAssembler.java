package id.kelompok2.miniproject.assembler;

import id.kelompok2.miniproject.model.dto.InputRequestDto;
import id.kelompok2.miniproject.model.dto.RequestDto;
import id.kelompok2.miniproject.model.entity.RequestEntity;
import org.springframework.stereotype.Component;

@Component
public class RequestAssembler implements InterfaceAssembler<RequestEntity, RequestDto> {
    @Override
    public RequestEntity fromDto(RequestDto dto) {
        if (dto == null) return null;

        RequestEntity entity = new RequestEntity();
        if (dto.getIdRequest() != null) entity.setIdRequest(dto.getIdRequest());
        if (dto.getJudulBuku() != null) entity.setJudulBuku(dto.getJudulBuku());
        if (dto.getNamaPengarang() != null) entity.setNamaPengarang(dto.getNamaPengarang());
        entity.setJumlahVote(dto.getJumlahVote() +1);

        return entity;
    }

    @Override
    public RequestDto fromEntity(RequestEntity entity) {
        if (entity == null)
            return null;

        return RequestDto.builder()
                .idRequest(entity.getIdRequest())
                .judulBuku(entity.getJudulBuku())
                .namaPengarang(entity.getNamaPengarang())
                .jumlahVote(entity.getJumlahVote())
                .build();
    }
}
