package id.kelompok2.miniproject.assembler;

import java.sql.Date;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.kelompok2.miniproject.configuration.DefaultResponse;
import id.kelompok2.miniproject.model.dto.PeminjamanDto;
import id.kelompok2.miniproject.model.entity.MemberEntity;
import id.kelompok2.miniproject.model.entity.PeminjamanEntity;
import id.kelompok2.miniproject.repository.MemberRepository;

@Component
public class PeminjamanAssembler implements InterfaceAssembler<PeminjamanEntity, PeminjamanDto>{
	@Autowired
	MemberRepository memberRepository;
	
	@Override
	public PeminjamanEntity fromDto(PeminjamanDto dto) {
		if (dto == null) return null;
		PeminjamanEntity entity = new PeminjamanEntity();
		if(dto.getJudulBuku() != null) entity.setJudulBuku(dto.getJudulBuku());
		LocalDate pinjam = LocalDate.now();
		entity.setTanggalPeminjaman(Date.valueOf(pinjam));
		LocalDate kembali = LocalDate.of(pinjam.getYear(),pinjam.getMonthValue(),pinjam.getDayOfMonth()).plusDays(7);
		entity.setTanggalPengembalian(Date.valueOf(kembali));
		entity.setKeteranganPinjam("Belum Dikembalikan");
//		entity.setKodePeminjaman("PJ-" + entity.getIdPeminjaman());
		return entity;
	}

	@Override
	public PeminjamanDto fromEntity(PeminjamanEntity entity) {
		if (entity == null) return null;
		return PeminjamanDto.builder()
				.idPeminjaman(entity.getIdPeminjaman())
				.username(entity.getUsername())
				.judulBuku(entity.getJudulBuku())
				.tanggalPeminjaman(entity.getTanggalPeminjaman())
				.tanggalPengembalian(entity.getTanggalPengembalian())
				.keteranganPinjam(entity.getKeteranganPinjam())
				.kodePeminjaman(entity.getKodePeminjaman())
				.build();
	}
	
}
