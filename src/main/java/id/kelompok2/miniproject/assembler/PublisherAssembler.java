package id.kelompok2.miniproject.assembler;

import org.springframework.stereotype.Component;

import id.kelompok2.miniproject.model.dto.PublisherDto;
import id.kelompok2.miniproject.model.entity.PublisherEntity;

@Component
public class PublisherAssembler implements InterfaceAssembler<PublisherEntity, PublisherDto> {

	@Override
	public PublisherEntity fromDto(PublisherDto dto) {
		// TODO Auto-generated method stub
		if (dto == null)
			return null;

		PublisherEntity entity = new PublisherEntity();
		if (dto.getIdPenerbit() != null)
			entity.setIdPenerbit(dto.getIdPenerbit());
		if (dto.getNamaPenerbit() != null)
			entity.setNamaPenerbit(dto.getNamaPenerbit());
		if (dto.getKontakPenerbit() != null)
			entity.setKontakPenerbit(dto.getKontakPenerbit());
		if (dto.getAlamatPenerbit() != null)
			entity.setAlamatPenerbit(dto.getAlamatPenerbit());

		return entity;
	}

	@Override
	public PublisherDto fromEntity(PublisherEntity entity) {
		// TODO Auto-generated method stub
		if (entity == null)
			return null;
		return PublisherDto.builder().idPenerbit(entity.getIdPenerbit()).namaPenerbit(entity.getNamaPenerbit())
				.kontakPenerbit(entity.getKontakPenerbit()).alamatPenerbit(entity.getAlamatPenerbit()).build();
	}

}
