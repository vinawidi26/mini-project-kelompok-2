package id.kelompok2.miniproject.assembler;

import id.kelompok2.miniproject.model.dto.JenisDto;
import id.kelompok2.miniproject.model.entity.JenisEntity;
import org.springframework.stereotype.Component;

@Component
public class JenisAssembler implements InterfaceAssembler<JenisEntity, JenisDto> {
    @Override
    public JenisEntity fromDto(JenisDto dto) {
        if (dto == null)
        return null;

        JenisEntity jenisEntity = new JenisEntity();
        if (dto.getIdJenis() != null) jenisEntity.setIdJenis(dto.getIdJenis());
        if (dto.getNamaJenis() != null) jenisEntity.setNamaJenis(dto.getNamaJenis());
        return jenisEntity;
    }

    @Override
    public JenisDto fromEntity(JenisEntity entity) {
        if (entity == null)
        return null;
        return JenisDto.builder()
                .idJenis(entity.getIdJenis())
                .namaJenis(entity.getNamaJenis())
                .build();
    }
}
